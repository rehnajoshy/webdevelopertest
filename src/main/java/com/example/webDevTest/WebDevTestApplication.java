package com.example.webDevTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class WebDevTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebDevTestApplication.class, args);
	}

}
