package com.example.webDevTest.nominatim;

import com.example.webDevTest.nominatim.dbBackedCache.Service.RestService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

@RestController
@EnableJpaRepositories
public class ReverseGeoCoderApiController {
    @Autowired
    RestService restService;
    @RequestMapping(value= "/myapp/get_address/{lat}/{lon}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<String>
    getAddress(@PathVariable("lat") float lat,
               @PathVariable("lon") float lon) throws IOException, ParserConfigurationException, SAXException, JSONException {

        String name = restService.getLocationName(lat, lon);
       return ResponseEntity.ok("{\"name\":" +"\""+name+ "\"}");
    }
}
