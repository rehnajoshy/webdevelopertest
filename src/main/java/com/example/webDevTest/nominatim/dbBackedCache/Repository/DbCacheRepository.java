package com.example.webDevTest.nominatim.dbBackedCache.Repository;

import com.example.webDevTest.nominatim.dbBackedCache.entity.DbCache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface DbCacheRepository extends JpaRepository<DbCache, Long> {
    DbCache findByLatAndLon(float lat, float lon);
    DbCache save(DbCache dbCache);
    @Query("SELECT id FROM DbCache dc WHERE dc.updatedAt < :before")
    List<Long> findByTime(Timestamp before);

    void deleteById(Long id);

}
