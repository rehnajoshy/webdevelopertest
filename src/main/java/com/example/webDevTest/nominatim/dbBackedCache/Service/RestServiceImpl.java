package com.example.webDevTest.nominatim.dbBackedCache.Service;

import com.example.webDevTest.nominatim.dbBackedCache.entity.DbCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

@Service
public class RestServiceImpl implements RestService {
    @Autowired
    CacheService cacheService;
    public String getLocationName(float lat, float lon) throws IOException, SAXException, ParserConfigurationException {

        DbCache dbCache = cacheService.getFromCache(lat,lon);

        String name;
        if(dbCache == null) {
            name = callNominatim(lat, lon);
            cacheService.saveInCache(lat,lon,name);
        }
        else {
            name = dbCache.getName();
            cacheService.updateCache(dbCache);
        }
        return name;

    }

    private String callNominatim(float lat, float lon) throws IOException, SAXException, ParserConfigurationException {
        String uri = "https://nominatim.openstreetmap.org/reverse?lat=" + lat + "&lon=" + lon;


        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(uri);

        // normalize XML response
        doc.getDocumentElement().normalize();
        return doc.getElementsByTagName("result").item(0).getTextContent();
    }

}
