package com.example.webDevTest.nominatim.dbBackedCache.Service;

import com.example.webDevTest.nominatim.dbBackedCache.entity.DbCache;

public interface CacheService {
    DbCache getFromCache(float lat, float lon);
    void saveInCache(float lat, float lon, String name);
    void updateCache(DbCache dbCache);
}
