package com.example.webDevTest.nominatim.dbBackedCache.Service;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public interface RestService {
    String getLocationName(float lat, float lon) throws IOException, SAXException, ParserConfigurationException;
}
