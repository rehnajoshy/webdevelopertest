package com.example.webDevTest.nominatim.dbBackedCache.scheduler;

import com.example.webDevTest.nominatim.dbBackedCache.Repository.DbCacheRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Component
public class DeleteCacheScheduler {

    private static final Logger logger = LogManager.getLogger(DeleteCacheScheduler.class);

    @Autowired
    DbCacheRepository dbCacheRepository;
    @Scheduled(fixedRateString = "${delete.cache.delay.prop}")
    public void deleteCache() {
       List<Long> deleteIds =  dbCacheRepository.findByTime(Timestamp.valueOf(LocalDateTime.now().minus(24, ChronoUnit.HOURS)));
       deleteIds.forEach(id -> {dbCacheRepository.deleteById(id); logger.info("deleted" + id);});
    }
}
