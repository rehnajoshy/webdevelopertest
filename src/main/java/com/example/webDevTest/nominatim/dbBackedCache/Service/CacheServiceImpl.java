package com.example.webDevTest.nominatim.dbBackedCache.Service;

import com.example.webDevTest.nominatim.dbBackedCache.Repository.DbCacheRepository;
import com.example.webDevTest.nominatim.dbBackedCache.entity.DbCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Service
public class CacheServiceImpl implements CacheService{
    @Autowired
    DbCacheRepository dbCacheRepository;
    @Override
    public DbCache getFromCache(float lat, float lon) {
        return dbCacheRepository.findByLatAndLon(lat,lon);
    }

    @Override
    public void saveInCache(float lat, float lon, String name) {
        DbCache dbCache = new DbCache();
        dbCache.setLat(lat);
        dbCache.setLon(lon);
        dbCache.setName(name);
        dbCacheRepository.save(dbCache);
    }

    @Override
    public void updateCache(DbCache dbCache) {
        dbCache.setUpdatedAt(Timestamp.valueOf(LocalDateTime.now()));
        dbCacheRepository.save(dbCache);
    }
}
