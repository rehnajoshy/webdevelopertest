
# Technologies Used
* Spring boot Maven
* Java 17
* Postgres 11


### Database configuration file:

src/main/resources/application.properties

### jar file

webDevTest-0.0.1-SNAPSHOT.jar

### To run 

* clone repo
* java -jar  webDevTest-0.0.1-SNAPSHOT.jar

application will run on 

http://localhost:8081

Example request: http://localhost:8081/myapp/get_address/-34.4391708/-58.7064573


#### application properties can be override while running.

For example  spring.datasource.username=postgres can be override by:

java  --spring.datasource.username=root -jar  webDevTest-0.0.1-SNAPSHOT.jar 


###Scheduler

A scheduler named DeleteCacheScheduler,  configured to run in every 10 seconds to delete entries from db which is not updated in last 24 hours. Its delay 10 seconds,  can be changed by replacing the value for delete.cache.delay.prop in application.properties which is in milliseconds. 


TODO:

1. Automated Testing.
2. Error handling.
3. Code refactoring.

